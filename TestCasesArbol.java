
import static org.junit.Assert.*;
import org.junit.Test;


public class TestCasesArbol {
	
	@Test 
	public void FirstTestCase(){
		
		Tree x = new Tree();
		x.InsertarNodo(1);
		x.InsertarNodo(2);
		x.InsertarNodo(3);
		x.InsertarNodo(4);
		x.InsertarNodo(5);
		x.InsertarNodo(6);
		
		assertEquals(true,x.sanityCheck(x));
	}
	
	@Test 
	public void FirstTestCase2(){
		
		Tree x = new Tree();
		x.InsertarNodo("a");
		x.InsertarNodo("b");
		x.InsertarNodo("v");
		x.InsertarNodo("d");
		x.InsertarNodo("e");
		x.InsertarNodo("f");
		
		assertEquals(true,x.sanityCheck(x));
	}
}
