

public class Tree {
	
	private Node root = null;
	
	public class Node {

		private Comparable key;
		private boolean color = false; 
		private Node left = null, right = null;
		private Node p = null;
		//Red = true Black = False
		

		public Node(Comparable x){
			key = x;
		}

		private boolean checkRoot(){
			return (p == null && color == false);
		}
		
		private boolean checkNodeRed(){
			if(color == true)
				return ((left == null || left.color == false) && (right == null || right.color == false));
			return true;
		}
		
		private int checkRBProperty(){
			
			if (left != null && left.checkRBProperty() == -1)
				return -1;
			if (right != null && right.checkRBProperty() == -1)
				return -1;
			if (left == null)
			   if (right != null && right.checkRBProperty() == -1)
				return 1;
			   else
				return -1;
			
			if(right == null)
				if (left == null || left.checkRBProperty() == 1)
					return 1;
			return -1;
		}
		
		
	}
	
	public void InsertarNodo(Comparable x){
		Node y = new Node(x);
		insert(y);
	}
	
	private void insert (Node z){
		
		assert(z != null);
		Node y = null;
		Node x = this.root;
		
		while(x != null){
			y = x;
			if(z.key.compareTo(x.key) == -1)
				x = x.left;
			else
				x = x.right;
		}
		z.p = y;
		if(y == null)
			this.root = z;
		else if (z.key.compareTo(y.key) == -1)
			y.left = z;
		else
			y.right = z;
		z.left = null;
		z.right = null;
		z.color = true;
	}
	
	private void leftRotate (Node x){
		assert(x.right != null): "Right son was null"+ x.key;
		
		Node y = x.right;
		x.right = y.left;
	
		if(y.left != null)
			y.left.p = x;
		y.p = x.p;
		
		if(x.p == null)
			root = y;
		else if (x == x.p.left)
			x.p.left = y;
		else
			x.p.right = y;
		y.left = x;
	}
	
	private void rightRotate(Node y){
		assert(y.left!=null):"left son was null" + y.key;

		System.out.println("sdfdf"+y.key);

		Node x=y.left;
		y.left=x.right;

		if(x.right!=null)
			x.right.p=y;
		x.p=y.p;
		if(y.p==null)
			root=x;
		else if(y==y.p.right)
			y.p.right=x;
		else
			y.p.left=x;
		x.right=y;
		y.p=x;

	}
	

	
	private void insertFixup(Node z){
		Node y;
		if(z.p!=null && z.p.p!=null){
			if(z.p.color==true){
				if(z.p==z.p.p.left){
					y= z.p.p.right;
					
					if(y!=null && y.color==true ){ 
						z.p.color=false;
						y.color=false;
						z.p.p.color=true;
						z=z.p.p;
					}else if(z==z.p.right){
						z=z.p;
						leftRotate(z);	
					}
					else{
						z.p.color=false;
						if(z.p.p!=null){
							z.p.p.color=true;
							rightRotate(z.p.p);
						}

					}
				}
			}
		}
	}
	
	public boolean sanityCheck(Tree tree)
	{
		if(tree.root.color == true)
			throw new RuntimeException("Error: color root"); 
		else if(checkProperty(tree.root) != 1)
			throw new RuntimeException("Error: every simple path has not the same number of black nodes");
		else
			System.out.println("OK");
		return true;
	}
	
	private int checkProperty(Node node) 
	{
		//Return 1 if checkProperty is OK
		if(node == null)
			return 1;
		else
		{
			if(node.color == true)
				if(node.left != null && node.left.color == true ||
					node.right != null && node.right.color == true)
					return 0;
			int s = 0;
			int izq; 
			int der;
			if(node.color == false)
				s = 1;
			
			izq = s + checkProperty(node.left);
			der = s + checkProperty(node.right);
			if(izq == der)
				return 1;
			return 0;
		}
	}
	
}
